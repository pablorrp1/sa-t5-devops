> Universidad de San Carlos \
Facultad de Ingeniería \
Escuela de Ciencias y Sistemas \
Sofware Avanzado\
Laboratorio

>Pablo Roberto Roche Palacios \
carnet: 2011-22998

---
# Tarea 5

Aplicación SOA que simule servicios de carros, tipo Uber.

- Solicitud de servicio por parte del cliente
- Recepción de solicitud y aviso a piloto
- Solicitud de ubicación (rastreo)

## 1. Semver
Versionamiento de la aplicación, según Semver.org, puede encontrarse en package.json

![Versionamiento](semver.jpg)

Nota: Actualmente está en la versión 2.1.0, por ser continuidad de la tarea 2-3, que puede encontrarse [acá][tarea2y3]

## 2. Ramas
![Ramas del Proyecto](ramas.jpg)

Se crean 2 ramas principales
-   Master: Contiene el código principal y funcional, toda vez pase las pruebas y la contrucción.
-   Develop: Contiene el código que se está desarrollando. Luego que se construya la solución, se envían los cambios a la rama Master

Luego existirán 3 tipos de reglas que organizarán el código:
-   Feature: Rama para agregar nuevas características y arreglos de bugs. Regularmente se encuentran en el repositorio local, y se unen a la rama Develop, al terminar.
-   Release: Rama con versiones menores y mayores del código. Regularmente se unen a las ramas Master y Develop. Crean una etiqueta para referencias futuras.
-   Hotfix: Rama similar a Release, excepto que son creadas para cambios menores, desde la rama Master. Al igual que la rama Release, se unen a las ramas Master y develop al terminar.

Más información del manejo de rams, en la [documentación de GitKraken][gitflow]

## 3. Construcción de artefactos
Al usar GitLab se tiene la ventaja de contar con Gitlab CI/CD.
Por el momento, la creación del artefacto, se construye desde el archivo .gitlab-ci.yml.

## 4. Revisión de Estándares
Para la revisión, se usa [JavaScript Standard Style][jss], para la correción de código.
Entre las reglas que revisa esta librería están:
- 2 espacios como sangría.
- Usar comillas simples en cadenas de texto con la excepción de escapado de texto
- No dejar variables sin usar – esta captura toneladas de bugs!
- Sin punto y coma – Está bien. ¡En serio!
- Nunca empezar una línea con (, [, o `  - Este es el único problema al evitar punto y coma – automáticamente verificado para ti!
- Más detalles
- Espacio después de las palabras claves if (condition) { ... }
- Espacio después del nombre de función function name (arg) { ... }
- Usar siempre  === en vez de == – pero obj == null está permitido para verificar null || undefined.
- Gestionar siempre el parámetro de función err de node.js
- Usar siempre el prefijo window en los globales del navegador – A excepción de document y navigator esto está bien
- Previene el uso accidental de mal-llamados globales del navegador como open, length, event, y name.

Según la documentación, se debe de instalar

```
npm install standard --global
```

Y luego se procede a analizar con

```
standard
```

Al correrlo por primera vez, se observaron estos errores:

![Errores, Horrores](standard.jpg)

Al correrlo después de los cambios, no se observa nada más: 

![Happy](standardfin.jpg)

---
# Tarea 6

Utilizar Netlify para automatizar el despliegue de los artefactos desde su repositorio

[Sitio de Netlify para artefactos][netlify]

---
# Tarea 7
1. Instalar componentes para pruebas unitarias. Incluir pruebas unitarias a los microservicios.
2. Hacer un despliegue con GitLab Auto DevOps que involucre
  a. Pruebas Unitarias
  b. Revision de calidad de código
  c. Construccion de artefactos

## Pruebas unitarias con [Mocha][mocha]

![Mocha](mocha.png)

Mocha es una herramienta de pruebas para NodeJS, permite ejecutar pruebas de manera síncrona, asíncrona y ordenada. Contiene funciones importantes:
1. 'Describe' : es el encargado de definir la estructura y agrupar las rpuebas unitarias.
2. 'It': es la que define las pruebas unitarias que se realizan

Se instalaron las dependencias
- mocha: framework de pruebas
- chai: librería de aserciones
- expect : interface de chai para las pruebas
- supertest: llama a las rutas, es más fácil que enviar peticiones HTTP

Para correr las pruebas, se modifica el archivo package.json:

```
"scripts": {
    ...
    "test": "mocha ./test/*.js --exit"
    ...
  },
```

Se agregaron archivos de test, que llaman al microservicio, y comprueban que devuelva un mensaje y un código de aceptación.

Para correr las pruebas, se ejecuta:
```
npm test
```

El resultado en consola es:

![Testeando](resultadotest.jpg)

## GitLab Auto DevOps
Se configura el archivo _.gitlab-ci.yml._ con 2 etapas:
1. Preconstruccion: Involucra la instalación de dependencias que ayudarán a ejecutar correctamente las pruebas.
2. Pruebas: Tiene la ejecución de las pruebas unitarias y la revisión de calidad de código.

Posterior a que un Pipeline sea exitoso, se construye el artefacto especificado en la Tarea 6


[tarea2y3]:https://github.com/pablorrp1/SA-T2-3-ESB
[gitflow]:https://support.gitkraken.com/git-workflows-and-extensions/git-flow/
[jss]:https://standardjs.com/readme-esla.html
[netlify]:https://keen-jennings-ec4df6.netlify.com/
[mocha]:https://mochajs.org/