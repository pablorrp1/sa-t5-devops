'use strict'

var client = require('./client.js')
var driver = require('./driver.js')

var controllers = {
  requestcar: function (req, res) {
    client.requestcar(req, res)
  },
  endcar: function (req, res) {
    client.endcar(req, res)
  },
  alertdriver: function (req, res) {
    driver.ring(req, res)
  }
}

module.exports = controllers
