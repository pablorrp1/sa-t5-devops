'use strict'

var controller = require('./controllers')

module.exports = function (app) {
  app.route('/request').get(controller.requestcar)
  app.route('/end').get(controller.endcar)
  app.route('/alert').get(controller.alertdriver)
}
