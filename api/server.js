var express = require('express')
var app = express()
var port = process.env.PORT || 3000

var routes = require('./routes')
routes(app)
app.listen(port, function () {
  console.log('Servidor en puerto: ' + port)
})

module.exports.app = app
