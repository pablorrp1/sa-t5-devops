var client = {
  requestcar: function (req, res) {
    res.setHeader('Content-Type', 'application/json')
    res.send({ msj: 'Pidiendo carro, favor de esperar' })
  },
  endcar: function (req, res) {
    res.setHeader('Content-Type', 'application/json')
    res.send({ msj: 'Terminando viaje, vuelva pronto' })
  }
}

module.exports = client
