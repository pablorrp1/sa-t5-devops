const request = require('supertest')
const expect = require('expect')
const app = require('../api/server').app

describe('user-test.js', () => {
  it('Check require', (done) => {
    request(app)
      .get('/request')
      .expect('Content-Type', /json/)
      .expect((res) => {
        expect(res.body).toHaveProperty('msj', 'Pidiendo carro, favor de esperar')
      })
      .end(done)
  })

  it('Check end', (done) => {
    request(app)
      .get('/end')
      .expect('Content-Type', /json/)
      .expect((res) => {
        expect(res.body).toHaveProperty('msj', 'Terminando viaje, vuelva pronto')
      })
      .end(done)
  })
})
