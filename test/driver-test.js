const request = require('supertest')
const expect = require('expect')
const app = require('../api/server').app

describe('driver-test.js', () => {
  it('Check alert', (done) => {
    request(app)
      .get('/alert')
      .expect('Content-Type', /json/)
      .expect((res) => {
        expect(res.body).toHaveProperty('msj', 'Un usuario te espera')
      })
      .end(done)
  })
})
